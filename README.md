# Sn-monitor Backend

Ce repo contient le code source de la backend de sn monitor.

## Exigence

- [node.js version 16.0.0 or higher](https://nodejs.org/en/)

## Structure

- **Dossier** `src/`: contient les differents traitements des entites et leur controlleurs.

---

- **Dossier** `log/`: contient les fichiers log.

---

- **Fichier** `server.ts`: c'est le point d'entrer de l'application (main).

---

- **Fichier** `app.ts`: contient la class App qui represent l'instance de **express js** qui va s'occuper de l'
  initialization de tout les modules (middleware,loggers,controllers,connection a la DB...) nécessaire pour le
  fonctionnement de l'application.

---

## Demarrage

- pour demarrer la backend il faut tout d'abord ajouter un fichier .env dans le path suivant `src/.env`
  apres il faut ajouter les variables d'environnement en se basant sur le fichier `.env_example`

#### variable d'environnement

- connection string de la base de donne mongodb<br/>
  `CONNECTION_STRING = mongodb://localhost/pfe`<br/>
- le secret pour le token JWT<br/>
  `SECRET=xxxx`<br/>
- adress email de l'admin<br/>
  `ADMIN_EMAIL=xxxx@xxx.xxx`<br/>
- mot de passe de l'admin<br/>
  `ADMIN_PASSWORD=xxxxxx`<br/>

la duree de vie du token JWT les formats disponible sont:

- 1d = 1 jour
- 1h = 1 heure
- 1s =1 seconde
  <br/>

`JWT_EXPTIME=1d`<br/>

- la duree de vie du token JWT si l'utilisateur a cliquer sur se souvenir de moi<br/>
  `JWT_EXPTIME_REMEMBER=7d`<br/>

apres avoir configurer le `.env` il reste qu'a entrer les commandes suivantes

```shell script
$> yarn install
```

en mode development

```shell script
$> yarn dev
```

en mode production, cette commande va lancer le tslint responsable a la verification des standard du code et apres le
build avec tsc et finalement le serveur avec nodemon

```shell script
$> yarn start
```

pour clean les fichiers build

```shell script
$> yarn clean
```
