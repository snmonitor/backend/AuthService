import Request from './Request';

import mongoose from 'mongoose';
import config from 'config'

const path = "/api/users"

const str = (Math.random() + 1).toString(36).substring(7);

let id = ""

const request = new Request()

beforeAll(async () => {
     await request.getToken()
     const url: string = await config.get("CONNECTION_STRING");
     await mongoose.connect(url)
})


afterAll(async () => {

     await mongoose.disconnect()
     await mongoose.connection.close()
})

describe("GET USERS", () => {
     test("users found",
          async () => {
               const res = await request.GET(path)

               expect(res.body.code).toBe(8)
          })
})





describe("CREATE USER", () => {

     test("email missing", async () => {
          const res: any = await request.POST(path, {
               "name": "test user",
          })
          expect(res.body.code).toBe(7)
     })


     test("mdp missing", async () => {
          const res = await await request.POST(path
               , {
                    "name": "test user",
                    "email": str,
               })

          expect(res.body.code).toBe(7)
     })

     test("email not valid", async () => {
          const res = await request.POST(path, {
               name: "test user",
               email: str,
               mdp: "123456"
          })

          expect(res.body.code).toBe(6)
     })

     test("invalid mdp", async () => {
          const res = await request.POST(path, {
               name: "test user",
               email: `${str}@akka.eu`,
               mdp: "12345"
          })
          expect(res.body.code).toBe(15)
     })

     test("user created", async () => {
          const res = await request.POST(path, {
               name: "test user",
               email: `${str}@akka.eu`,
               mdp: "123456"
          })
          id = res.body.donne._id
          expect(res.body?.code).toBe(3)

     })

     test("email already used", async () => {
          const res = await request.POST(path, {
               name: "test user",
               email: `${str}@akka.eu`,
               mdp: "123456"
          })
          expect(res.body.code).toBe(5)
     })
})


describe("UPDATE USER", () => {

     test("_id missing", async () => {
          const res = await request.PUT(path, {
               "name": "test user",
          })
          expect(res.body.code).toBe(7)
     })
     test("email missing but should update", async () => {
          const res = await request.PUT(path, {
               _id: id,
               "name": "test user",
          })
          expect(res.body.code).toBe(17)
     })


     test("email not valid", async () => {
          const res = await request.PUT(path, {
               _id: id,
               name: "test user",
               email: str,
               mdp: "123456"
          })
          expect(res.body.code).toBe(6)
     })

     test("invalid mdp", async () => {
          const res = await request.PUT(path, {
               _id: id,
               name: "test user",
               mdp: "12345"
          })
          expect(res.body.code).toBe(15)
     })

     test("email already used", async () => {
          const res = await request.PUT(path, {
               _id: id,
               name: "test user",
               email: `admin@akka.eu`,
               mdp: "123456"
          })
          expect(res.body.code).toBe(5)
     })

     test("user updated", async () => {
          const res = await request.PUT(path, {
               _id: id,
               name: "test user",
               email: `${str}1@akka.eu`,
               mdp: "123456"
          })
          expect(res.body.code).toBe(17)
     })


})



describe("GET USER BY ID", () => {

     test("id is not an uuid", async () => {
          const res = await request.GET(`${path}/ffff`)

          expect(res.body.code).toBe(14)
     })

     test("user not found", async () => {
          const res = await request.GET(`${path}/6228060021100c60112191df`)

          expect(res.body.code).toBe(12)
     })

     test("user found", async () => {
          const res = await request.GET(`${path}/${id}`)

          expect(res.body.code).toBe(8)
     })
})

describe("DELETE USER", () => {


     test("id provided not uuid", async () => {
          const res = await request.DELETE(`${path}/ffff`)

          expect(res.body.code).toBe(14)
     })

     test("user not found", async () => {
          const res = await request.DELETE(`${path}/6256c285276b394196c2ee8f`)

          expect(res.body.code).toBe(12)
     })

     test("user DELETED", async () => {
          const res = await request.DELETE(`${path}/${id}`)

          expect(res.body.code).toBe(4)
     })
})