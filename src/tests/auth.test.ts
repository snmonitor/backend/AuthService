import Request from './Request';
import mongoose from 'mongoose';
import config from 'config'

const path = "/api/users/authenticate"


const request=new Request()

beforeAll(async () => {
     await request.getToken()
     const url:string = await config.get("CONNECTION_STRING");
     await mongoose.connect(url)
})


afterAll(async () => {
     await mongoose.disconnect()
     await mongoose.connection.close()
})

describe("AUTHENTICATE", () => {
     
     test("wrong password",
          async () => {
               const res = await request.POST(path, {
	email: await config.get("ADMIN_EMAIL"),
	password:  "ffefefefsa",
	remember:"true"
               })
               
               expect(res.body.code).toBe(9)
          })
     
     test("wrong email",
          async () => {
               const res = await request.POST(path, {
	email: "wdwdwd",
	password:  await config.get("ADMIN_PASSWORD"),
	
               })
               
               expect(res.body.code).toBe(9)
          })
     
      test("missing credential",
          async () => {
               const res = await request.POST(path, {
	          remember:"true"
               })
               expect(res.body.code).toBe(7)
          })
     
     
     test("remember me true",
          async () => {
               const res = await request.POST(path, {
	email: await config.get("ADMIN_EMAIL"),
	password:  await config.get("ADMIN_PASSWORD"),
	remember:true
               })
               expect(res.body.code).toBe(1)
               expect(res.body.param[0]).toBe(await config.get("JWT_EXPTIME_REMEMBER"))
          })
     
     test("remember me false",
          async () => {
               const res = await request.POST(path, {
	email: await config.get("ADMIN_EMAIL"),
	password:  await config.get("ADMIN_PASSWORD"),
	remember:"false"
               })
               
               expect(res.body.code).toBe(1)
               expect(res.body.param[0]).toBe(await config.get("JWT_EXPTIME"))
  })
})