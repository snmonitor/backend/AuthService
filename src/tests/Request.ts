import App from '../app';
import { EmailController, UserController } from "../controllers";
import supertest from 'supertest';
import config from 'config'


export default class Request {

     app = new App([UserController, EmailController]).app;
     token: string = "Bearer ";
     request = supertest(this.app)
     async getToken() {
          this.token = `Bearer ${config.get("API_TOKEN")}`
     }
     async GET(path: string) {
          return this.request.get(path).set({"Authorization": this.token});
     }

     async POST(path: string, body: any) {
          return  this.request.post(path).send(body).set({ "Authorization": this.token })
     }
     async DELETE(path: string, body = {}) {
          return  this.request.delete(path).send(body).set({ "Authorization": this.token })
     }

     async PUT(path: string, body = {}) {
          return  this.request.put(path).send(body).set({ "Authorization": this.token })
     }


}