import express, {Request, Response} from 'express'
import {Controller} from "../Controller";
import {UserService} from "../services";
import {HttpException, InternalServerError500Response} from "../responses";
import {UserRole} from "../enums/UserRole";
import {authorize} from '../middlewares/authMiddleware'

const userController = express.Router()

const userService = new UserService();

userController.route('/')
    .get(authorize([UserRole.admin]), async (_req: Request, res: Response) => {
        try {
            const response: HttpException = await userService.getUser()
            response.send(res);
        } catch (e:any) {
            new InternalServerError500Response(e.message).send(res);
        }
    })
    .post(authorize([UserRole.admin]), async (req: Request, res: Response) => {
        try {
            const response: HttpException = await userService.createUser(req.body)
            response.send(res);
        } catch (e:any) {
            new InternalServerError500Response(e.message).send(res);
        }
    })
    .put(authorize([UserRole.admin]), async (req: Request, res: Response) => {
        try {
            const response: HttpException = await userService.updateUser(req.body)
            response.send(res);
        } catch (e:any) {
            new InternalServerError500Response(e.message).send(res);
        }
    })

userController.route('/authenticate')
    .post(async (req: Request, res: Response) => {
        try {
            const response: HttpException = await userService.authenticate(req.body.email, req.body.password?.toString(), req.body.remember)
            response.send(res);

        } catch (e:any) {
            new InternalServerError500Response(e.message).send(res);
        }
    })

userController.route('/me')
    .get(authorize(), async (req: any, res: any) => {
        try {
            const response: HttpException = await userService.getUserById(req.auth?._id)
           response.send(res);
            
        } catch (e:any) {
            new InternalServerError500Response(e.message).send(res);
        }
    })

userController.route('/:id')
    .get(authorize([UserRole.admin]), async (req: Request, res: Response) => {
        try {
            const response: HttpException = await userService.getUserById(req.params.id)
            response.send(res);
        } catch (e:any) {
            new InternalServerError500Response(e.message).send(res);
        }
    })
    .put(authorize([UserRole.admin]), async (req: Request, res: Response) => {
        try {
            const response: HttpException = await userService.updateUser({_id: req.params.id, ...req.body})
            response.send(res);
        } catch (e:any) {
            new InternalServerError500Response(e.message).send(res);
        }
    }).delete(authorize([UserRole.admin]), async (req: Request, res: Response) => {
    try {
        const response: HttpException = await userService.deleteUser(req.params.id)
        response.send(res);
    } catch (e:any) {
        new InternalServerError500Response(e.message).send(res);
    }
})

userController.route('*')
.delete( async (_req: Request, res: Response) => {

        new InternalServerError500Response().send(res);
})


export const UserController = new Controller('users', userController)
