//process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
import App from './app';
import {
     EmailController, UserController
} from "./controllers";


const port = 1111;

const server = new App([UserController, EmailController,]);

server.start(port, `server available on http://localhost:${port}/api`);

