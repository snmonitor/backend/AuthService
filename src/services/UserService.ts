import {UserRepository} from "../repositories";
import {isValidObjectId} from "mongoose";
import jwt from 'jsonwebtoken'
import config from 'config'
import {
    Authenticated200Response,
    Created201Response,
    Deleted200Response,
    EmailAlreadyUsed400Response,
    EmailNotValide400Response,
    FieldMissing400Response,
    Found200Response,
    HttpException,
    IncorrectCredential401Response,
    NotFound404Response,
    NotUUID404Response,
    PasswordTooShort400Response,
    Updated200Response
} from "../responses";
import { isNullEmptyOrWhiteSpace, ValidateEmail} from "../helper";
import {IUser} from "../interfaces";


export class UserService {

    private secret:string = config.get('SECRET') ?? '';
    private userRepository = new UserRepository();

    async getUserById(id: string): Promise<HttpException> {
        if (isValidObjectId(id)) {
            const user = await this.userRepository.getUserById(id);
            if (user !== null) {
                return new Found200Response('user', 1, user);
            } else {
                return new NotFound404Response(id, 'user');
            }
        } else
            return new NotUUID404Response();
    }

    async getUser(): Promise<HttpException> {
        const users: IUser[] = await this.userRepository.getUsers()
        return new Found200Response('user', users.length ?? 0, users)
    }

    async createUser(data: { [key: string]: any }): Promise<HttpException> {

        // all required field check
        const check = this.checkRequired(data)
        if (check === 'true') {
            if (data.mdp?.length > 5) {
                // ensure that the email is written in a good format
                if (ValidateEmail(data.email)) {
                    // ensure that the email is not used
                    const u = await this.userRepository.getUserByEmail(data.email);
                    if (u === null) {
                        // hashing the password
                        const user = await this.userRepository.createUser(data);
                        return new Created201Response('user', user);
                    } else
                        return new EmailAlreadyUsed400Response(u.email)
                } else
                    return new EmailNotValide400Response(data.email)
            } else
                return new PasswordTooShort400Response()
        } else
            return new FieldMissing400Response(check)

    }

    async updateUser(data: { [key: string]: any }): Promise<HttpException> {
        if (data._id) {
            if (isValidObjectId(data._id)) {
                if (!data.mdp || data.mdp?.length > 5) {
                    // ensure that the email is written in a good format
                    if (!data.email || ValidateEmail(data.email)) {
                        const u = await this.userRepository.getUserByEmail(data.email);
                        if (u === null) {
                        const user = await this.userRepository.userExist(data._id)
                        if (user) {
                            const res = await this.userRepository.patchUser(data);
                            if (res !== null)
                                return new Updated200Response('user',Object.assign(res,data))
                        }
                        return new NotFound404Response(data._id, 'user');
                    }else {
                            return new EmailAlreadyUsed400Response(data.email)
                        }
                    } else {
                        return new EmailNotValide400Response(data.email)
                    }
                } else {
                    return new PasswordTooShort400Response()
                }
            } else {
                return new NotUUID404Response();
            }
        } else {
            return new FieldMissing400Response()
        }
    }

    async deleteUser(id: string): Promise<HttpException> {
        if (id) {
            if (isValidObjectId(id)) {
                const res: object = await this.userRepository.deleteUser(id);
                if (res !== null)
                    return new Deleted200Response('user', {_id: id})
                else
                    return new NotFound404Response(id, 'user');
            } else
                return new NotUUID404Response();
        } else
            return new FieldMissing400Response()
    }

    async authenticate(email: string, password: string, remember="true") {
        const field=this.checkAuthRequired({email,password});
        //check required
        if (field=="true")
        {const user = await this.userRepository.getUserByEmail(email);
        if (user && user.validPassword(password)) {
            // create the token
            const tokenExp:string = remember.toString()=="true" ? await config.get('JWT_EXPTIME_REMEMBER') : await config.get('JWT_EXPTIME')
            const token = jwt.sign({_id: user._id, role: user.role}, this.secret, {
                expiresIn: tokenExp
            });
            const userWithoutMdp = Object.assign(user, user, {mdp: undefined, salt: undefined, accessToken: token});

            await this.userRepository.patchUser(userWithoutMdp)
            return new Authenticated200Response(`${tokenExp}`,
                userWithoutMdp
            );

        } else
                return new IncorrectCredential401Response()
        }
        else return new FieldMissing400Response(field)
    }

    public checkRequired(json: { [key: string]: any }): string {

        if (isNullEmptyOrWhiteSpace(json.email))
            return 'email'
        if (isNullEmptyOrWhiteSpace(json.mdp))
            return 'mdp'
        if (isNullEmptyOrWhiteSpace(json.name))
            return 'name'

        return 'true'
    }

    public checkAuthRequired(json: { [key: string]: any }): string {

        if (isNullEmptyOrWhiteSpace(json.email))
            return 'email'
        if (isNullEmptyOrWhiteSpace(json.password))
            return 'password'

        return 'true'
    }


}