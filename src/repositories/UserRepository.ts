import {UserModel} from '../schemas'

export class UserRepository {

    private userModel = UserModel;

    async getUserById(id: string): Promise<any> {
        return this.userModel.findById(id);
    }

    async getUserByEmail(email: string): Promise<any> {
        return this.userModel.findOne({email})

    }

    async userExist(id: string) {
        return this.userModel.findById(id).select('_id');
    }


    async getUsers(): Promise<any> {
        return this.userModel.find();
    }

    async createUser(data: { [key: string]: any }): Promise<any> {
        const user = new this.userModel(data);
        await user.save();
        return user
    }

    async patchUser(data: { [key: string]: any }): Promise<any> {
        return this.userModel.findOneAndUpdate({_id: data._id}, data);
    }

    async deleteUser(id: string): Promise<any> {
        return this.userModel.findOneAndDelete({_id: id});
    }


}