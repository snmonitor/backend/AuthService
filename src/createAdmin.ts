import {UserService} from "./services";
import {UserRole} from "./enums/UserRole";
import config from 'config'

const userService = new UserService();

export default async function createAdmin() {
    const user = {
        email: config.get('ADMIN_EMAIL'),
        mdp: config.get('ADMIN_PASSWORD'),
        name: "admin",
        role: UserRole.admin
    }

    const res = await userService.createUser(user)
    if (res.status >= 300) {
        throw res.message
    }
}