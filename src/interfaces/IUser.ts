import { UserRole } from "../enums/UserRole.js";

export interface IUser {
    name: string;
    email: string;
    mdp: string;
    accessToken?: string;
    salt: string
    role?: UserRole;
    status?: string
    emailVerified?: boolean;

}





