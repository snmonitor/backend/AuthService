import {IDriver} from "../interfaces"

export interface IProduct {
    name: string;
    familyUuid: string;
    drivers: [IDriver];
}





