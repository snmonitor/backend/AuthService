import {IConstructor, IVersion} from "../interfaces"

export interface IDriver {
    IP: string;
    port:string;
    name: string;
    retries:string;
    timeout:string;
    maxOid:string;
    delay:string;
    templateID:string;
    manufacturer:[IConstructor];
    version: IVersion;
    userModified: boolean;
    scannable: boolean;
    zipVersion: string;
    subDrivers: [IVersion]
}





