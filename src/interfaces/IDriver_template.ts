import {IVersion,IConstructor} from "../interfaces"

export interface IDriverTemplate {

    name: string;
    versions: [IVersion],
    manufacturer:[IConstructor]
    userModified: boolean;
    zipVersion: string;
    subDrivers: [IVersion]
    driversId: [string]
}





