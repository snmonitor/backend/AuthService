import { IValue } from "../interfaces";

export interface IAction {
    description: string,
    valueType: string,
    values: [IValue]
}





