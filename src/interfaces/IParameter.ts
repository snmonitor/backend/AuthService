export interface IParameter {
    defaultValue: string;
    groupName: string;
    availableValues: [string]
    comment: string
    name: string
    type: string
    mandatory: boolean
    secret: boolean
}





