import {IAction, IIdentification, IIndicator, IInformation,
    IMeter, IParameter, IResource, IUnit}
    from "../interfaces"

export interface IVersion {
    name: string;
    unitName: IUnit;
    unitIdentifier: IUnit;
    resources: [IResource];
    infos: [IInformation],
    indicators: [IIndicator],
    meters: [IMeter],
    actions: [IAction],
    driverName: string,
    driverUuid: string,
    parameters: [IParameter];
    rootSubdriverUuid: string;
    url: string
    driverVersion: string
    protocol: string
    maxRefreshFrequency: string
    identification: [IIdentification];
    children: [string]

}





