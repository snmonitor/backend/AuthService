import {model, Schema} from "mongoose";
import {IUser} from "../interfaces"
import crypto from 'crypto';
import {UserRole} from "../enums/UserRole";

export const UserSchema: Schema<IUser> = new Schema<IUser>(
    {
        name: {type: String, required: true},
        email: {type: String, required: true, unique: true},
        mdp: {type: String, required: true},
        salt: String,
        accessToken: String,
        role: {type: String, default: UserRole.standard},
        status: String,
        emailVerified: Boolean,
    }
    , {timestamps: true}
);

UserSchema.pre(/save/, function (next: any) {

    // Creating a unique salt for a particular user
    this.salt = crypto.randomBytes(16).toString('hex');

    // Hashing user's salt and password with 1000 iterations,
    this.mdp = crypto.pbkdf2Sync(this.mdp, this.salt,
        1000, 64, `sha512`).toString(`hex`);
    next();
});

UserSchema.pre('findOneAndUpdate', function (next: any) {
    const data: any = this.getUpdate() ?? {}
    if (data.mdp) {
        // Creating a unique salt for a particular user
        data.salt = crypto.randomBytes(16).toString('hex');

        // Hashing user's salt and password with 1000 iterations,
        data.mdp = crypto.pbkdf2Sync(data.mdp, data.salt,
            1000, 64, `sha512`).toString(`hex`);
    }
    next();
});

UserSchema.methods.validPassword = function (password: string) {
    const mdp = crypto.pbkdf2Sync(password,
        this.salt, 1000, 64, `sha512`).toString(`hex`);
    return this.mdp === mdp;
};

export const UserModel = model('User', UserSchema, 'User')