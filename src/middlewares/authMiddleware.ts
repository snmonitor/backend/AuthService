import {expressjwt as jwt} from "express-jwt";
import {NextFunction,Response,Request} from "express";
import {JWTExpired401Response, NotAllowed403Response, Unauthorized401Response} from "../responses";
import {UserRepository} from "../repositories";
import { isNullEmptyOrWhiteSpace } from "../helper";
import config from 'config'

const secret:string = config.get('SECRET') ?? 'secret';

const userRepository = new UserRepository()

export function authorize(roles: string[] = []) {

    return [
        jwt({secret:isNullEmptyOrWhiteSpace(secret)?"secret":secret, algorithms: ['HS256'],}),
        // authorize based on user role
        async (err:Error,req:Request, res:Response, next:NextFunction) => {
            if(err.message=='jwt expired')
            return new JWTExpired401Response().send(res);
            else
            return new Unauthorized401Response(err.message).send(res);  
        },
         // authorize based on user role
        async (req:Request, res:Response, next:NextFunction) => {
                // check if the user exist
            if (await userRepository.userExist((req as any).auth?._id) !== null) {
                if (roles.length && !roles.includes((req as any).auth?.role)) {
                   return new NotAllowed403Response({_id:(req as any).auth?._id}).send(res)
                }
                // authentication and authorization successful
                next()
        }
        }
    ];

}