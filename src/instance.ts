import winston from 'winston';
import mongoose from 'mongoose';
import winstonRotate from 'winston-daily-rotate-file';
import formData from 'form-data';
import mailgun from "mailgun.js"
import { isNullEmptyOrWhiteSpace } from './helper';
import config from 'config'

class Instance {

    private static _instance: Instance;
    private readonly _logger: winston.Logger;
    private readonly _mailgun;

    private constructor() {
  const key:string=config.get("MAILGUN_KEY")
        this._mailgun = new mailgun(formData).client({
            username: "api",
            key: isNullEmptyOrWhiteSpace(key)?"key":key
        });


        const fileTransport: winstonRotate = new winstonRotate({
            filename: './log/%DATE%.log',
            datePattern: 'YYYY-MM-DD',
            format: winston.format.combine(
                winston.format.colorize(),
                winston.format.timestamp(),
                winston.format.simple(),
            ),
        });

        this._logger = winston.createLogger({
            level: "silly",
            transports: [
                new winston.transports.Console({
                    format: winston.format.combine(
                        winston.format.colorize(),
                        winston.format.timestamp(),
                        winston.format.simple(),
                    ),
                }),
                fileTransport

            ],

        });
        winston.addColors({
            error: 'red',
            warn: 'yellow',
            info: 'cyan',
            http: 'green',
            debug: "orange",
            verbose:"magenta",
            silly: "blue",
        });
        const connectionString: string = config.get("CONNECTION_STRING")

        mongoose.connect(connectionString,
        ).then(() => this._logger.info('MongoDB connection established successfully'))
            .catch((e: any) => this._logger.error(`MongoDB connection failed with error: ${e}`));
    }

    public get logger() {
        return this._logger;
    }

    public get mailgun() {
        return this._mailgun;
    }

    static get() {
        if (this._instance) {
            return this._instance;
        }

        this._instance = new Instance();
        return this._instance;
    }
}

export default Instance;