import {HttpException} from "./index";

export class Created201Response extends HttpException {
    constructor(str: string = "", data: object) {
        super([str],3, 201, `${str} created`, data);
    }
}

