import {HttpException} from "./index";

export class NotUUID404Response extends HttpException {
    constructor(field: string = '_id') {
        super([field],14, 404, `the ${field} provided is incorrect`);
    }
}