import {HttpException} from "./index";

export class NotAllowed403Response extends HttpException {
    constructor(data={}) {
        super([],11, 403, `Access not allowed`,data);
    }
}

