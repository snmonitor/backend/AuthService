import {HttpException} from "./index";

export class Updated200Response extends HttpException {
    constructor(str='object', data: object) {
        super([str],17, 200, `${str} updated`, data);
    }
}