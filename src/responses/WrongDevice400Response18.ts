import {HttpException} from "./index";

export class WrongDevice400Response extends HttpException {
    constructor(ip="0.0.0.0",port="161") {
        super([ip,port],18, 400,`Connecting with the wrong host : ${ip} ; port : ${port}`);
    }
}