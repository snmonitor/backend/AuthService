import {HttpException} from "./index";

export class FieldMissing400Response extends HttpException {
    constructor(field: string = "_id") {
        super([field],7, 400, `the field ${field} must be provided`);
    }
}