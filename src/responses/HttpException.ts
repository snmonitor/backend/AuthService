import {Response} from "express";
import Instance from "../instance";
import {isIterable} from "../helper";

export class HttpException {

    readonly code: number;
    readonly message: string;
    readonly donne: { [key: string]: any };
    readonly status: number;
    readonly pagination:{ [key: string]: any }
    readonly param:any

    constructor(param:any=[],code: number=2, status: number=400, message: string="", donne: object = {},pagination={}) {
        this.code = code;
        this.status = status
        this.message = message;
        this.donne = donne;
        this.pagination=pagination
        this.param=param;
    }

    send(res: Response) {
        let id = '';
        if (isIterable(this.donne))
            id += this.donne?.map((el: { [key: string]: any; }) => {
                return ` ${(el as { [key: string]: any })._id}`
            })
        else
            id = this.donne._id

        if(process.env.NODE_ENV!=="test")
       { if (this.status < 300)
            Instance.get().logger.info(`${this.message}, ${id}, status: ${this.status}, code: ${this.code},`)
        else if(this.status==500) {
           Instance.get().logger.error(`${this.message}, ${this.donne?._id}, status: ${this.status}, code: ${this.code},`)
       }else if (this.status >= 300)
            Instance.get().logger.warn(`${this.message}, ${this.donne?._id}, status: ${this.status}, code: ${this.code},`)
}
        res.status(this.status).send({
            message: this.message,
            code: this.code,
            status: this.status,
            pagination:this.pagination,
            donne: this.donne,
            param:this.param
        })
    }

}

