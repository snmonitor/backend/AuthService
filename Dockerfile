FROM node:16 AS build
WORKDIR /app
COPY package.json ./
RUN npm install
COPY . ./
RUN npm run build

## prod
FROM node:16
WORKDIR /app
COPY package.json ./
COPY config ./config
RUN npm install --only=production
COPY --from=build /app/build ./
EXPOSE 1111
CMD ["npm","start"]